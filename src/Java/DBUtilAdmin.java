package Java;

import java.sql.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

public class DBUtilAdmin extends DBUtil {

    private String URL;
    private String name;
    private String password;

    public DBUtilAdmin(String URL) {
        this.URL = URL;
    }


    @Override
    public List<Reservation> getReservation() throws Exception {
        List<Reservation> reservations = new ArrayList<>();
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            // polaczenie z BD
            conn = DriverManager.getConnection(URL, name, password);

            // wyrazenie SQL
            String sql = "SELECT * FROM reservation";
            statement = conn.createStatement();
            // wykonanie wyrazenia SQL
            resultSet = statement.executeQuery(sql);
            // przetworzenie wyniku zapytania
            while (resultSet.next()) {
                // pobranie danych z rzedu
                int id = resultSet.getInt("id");
                String imieNazwizko = resultSet.getString("imieNazwisko");
                String email = resultSet.getString("email");
                String room = resultSet.getString("room");
                java.sql.Date data_odSQL = resultSet.getDate("data_od");
                java.util.Date data_odDate =  new java.util.Date(data_odSQL.getTime());
                LocalDate data_od = data_odDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                java.sql.Date data_doSQL = resultSet.getDate("data_do");
                java.util.Date data_doDate =  new java.util.Date(data_doSQL.getTime());
                LocalDate data_do = data_doDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                //przetworzenie
                reservations.add(new Reservation(id,imieNazwizko, email, room, data_od, data_do));


            }
        } finally {
            close(conn, statement, resultSet);
        }
        return reservations;

    }
    public void deleteReservation(String id) throws Exception {

        Connection conn = null;
        PreparedStatement statement = null;

        try {

            // konwersja id na liczbe
            int reservationID = Integer.parseInt(id);

            // polaczenie z BD
            conn = DriverManager.getConnection(URL, name, password);

            // zapytanie DELETE
            String sql = "DELETE FROM reservation WHERE id =?";

            statement = conn.prepareStatement(sql);
            statement.setInt(1, reservationID);

            // wykonanie zapytania
            statement.execute();

        } finally {

            // zamkniecie obiektow JDBC
            close(conn, statement, null);

        }

    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}