package Java;

import java.util.HashMap;

public class Room {

    private String room;


    //metoda liczaca cene za pokoj
    public double cenaZaPokoj(){

        HashMap <String,Double> map = new HashMap<>();
        map.put("1-os.",349.99);
        map.put("2-os.",599.99);
        map.put("4-os.",799.99);

        return map.get(room).doubleValue();
    }

    public Room() {
    }

    public Room(String room) {
        this.room = room;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }
}

