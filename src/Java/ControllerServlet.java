package Java;

import Java.Client;
import Java.Reservation;
import Java.Room;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;


@WebServlet("/ControllerServlet")
public class ControllerServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8"); // polskie znaki

        Client client = new Client(request.getParameter("imie_nazwisko"),request.getParameter("e-mail"));
        Room room = new Room(request.getParameter("rodzaj_pokoju"));
        //LocalDate data_od = LocalDate.parse(request.getParameter("data_od"));
        //LocalDate data_do = LocalDate.parse(request.getParameter("data_do"));

        Reservation reservation = new Reservation(request.getParameter("imie_nazwisko"),request.getParameter("e-mail"),request.getParameter("rodzaj_pokoju"), LocalDate.parse(request.getParameter("data_do")),LocalDate.parse(request.getParameter("data_od")));

        ArrayList<Reservation> info = new ArrayList<>();
        if(reservation.cenaZaPobyt()>0) {
           info.add(reservation);
        }

        request.setAttribute("info",info);

        //wysłanie do jsp
        RequestDispatcher dispatcher = request.getRequestDispatcher("/reservation_view.jsp");

        dispatcher.forward(request, response);

    }

    }
