package Java;

public class Client {

    private String imieNazwisko;
    private String email;

    public Client() {
    }

    public Client(String imieNazwisko, String email) {
        this.imieNazwisko = imieNazwisko;
        this.email = email;
    }

    public String getImieNazwisko() {
        return imieNazwisko;
    }

    public void setImieNazwisko(String imieNazwisko) {
        this.imieNazwisko = imieNazwisko;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
