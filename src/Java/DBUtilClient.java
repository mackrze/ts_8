package Java;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DBUtilClient extends DBUtil {

    private DataSource dataSource;

    public DBUtilClient(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Reservation> getReservation() throws Exception {
        List<Reservation> reservations = new ArrayList<>();
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {

            // polaczenie z BD
            conn = dataSource.getConnection();

            // wyrazenie SQL
            String sql = "SELECT * FROM reservation";
            statement = conn.createStatement();
            // wykonanie wyrazenia SQL
            resultSet = statement.executeQuery(sql);
            // przetworzenie wyniku zapytania
            while (resultSet.next()) {
                // pobranie danych z rzedu
                int id = resultSet.getInt("id");
                String imieNazwizko = resultSet.getString("imieNazwisko");
                System.out.println("get reservation dbutil jest while imie="+imieNazwizko);
                String email = resultSet.getString("email");
                String room = resultSet.getString("room");
                java.sql.Date data_odSQL = resultSet.getDate("data_od");
                Date data_odDate =  new Date(data_odSQL.getTime());
                LocalDate data_od = data_odDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                java.sql.Date data_doSQL = resultSet.getDate("data_do");
                Date data_doDate =  new Date(data_doSQL.getTime());
                LocalDate data_do = data_doDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

                //przetworzenie
                reservations.add(new Reservation(id, imieNazwizko, email,room, data_od, data_do));


            }
        } finally {
            close(conn, statement, resultSet);
        }
        return reservations;

    }

    public void addReservation(Reservation reservation) throws Exception {

        Connection conn = null;
        PreparedStatement statement = null;

        try {
            ZoneId defaultZoneId = ZoneId.systemDefault();
            // polaczenie z BD
            conn = dataSource.getConnection();

            // zapytanie INSERT i ustawienie jego parametrow
            String sql = "insert into reservation (imieNazwisko,email,room,data_od,data_do) "+
                    " values (?,?,?,?,?)";

            statement = conn.prepareStatement(sql);
            statement.setString(1, reservation.getImieNazwisko());
            statement.setString(2, reservation.getEmail());
            statement.setString(3, reservation.getRoom());
            Date data_od =  Date.from(reservation.getData_od().atStartOfDay(defaultZoneId).toInstant());
            java.sql.Date data_odSQL = new java.sql.Date(data_od.getTime());
            statement.setDate(4,  data_odSQL);
            Date data_do =  Date.from(reservation.getData_do().atStartOfDay(defaultZoneId).toInstant());
            java.sql.Date data_doSQL =  new java.sql.Date(data_do.getTime());
            statement.setDate(5, data_doSQL);

            // wykonanie zapytania
            statement.execute();


        } finally {

            close(conn, statement, null);

        }

    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
