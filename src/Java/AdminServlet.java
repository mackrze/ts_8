package Java;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
    private DBUtilAdmin dbUtil;
    private final String db_url = "jdbc:mysql://localhost:3306/lista8TS?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        try {

            dbUtil = new DBUtilAdmin(db_url);

        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {

        response.setContentType("text/html");

        String name = request.getParameter("name");
        String password = request.getParameter("password");

        dbUtil.setName(name);
        dbUtil.setPassword(password);

        if (validate(name, password)) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/admin_view.jsp");

            List<Reservation> reservations = null;

            try {

                reservations = dbUtil.getReservation();

            } catch (Exception e) {
                e.printStackTrace();
            }

            // dodanie listy do obiektu zadania
            request.setAttribute("RESERVATIONS_LIST", reservations);

            dispatcher.forward(request, response);
        } else {

            RequestDispatcher dispatcher = request.getRequestDispatcher("/admin_login.html");
            dispatcher.include(request, response);
        }


    }
    private boolean validate(String name, String pass) {
        boolean status = false;

        try {

            Class.forName("com.mysql.cj.jdbc.Driver");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();

        }

        Connection conn = null;

        try {

            conn = DriverManager.getConnection(db_url, name, pass);
            status = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {


        try {

            // odczytanie zadania
            String command = request.getParameter("command");

            if (command == null)
                command = "LIST";

            switch (command) {

                case "LIST":
                    listReservations(request, response);
                    break;


                case "DELETE":
                    deleteReservation(request, response);
                    break;

                default:
                    listReservations(request, response);
            }

        } catch (Exception e) {
            throw new ServletException(e);
        }

    }
    private void listReservations(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // pobranie danych dzieki DBUtil
        List<Reservation> reservations = dbUtil.getReservation();

        // dodanie telefonow do zadania
        request.setAttribute("RESERVATIONS_LIST", reservations);

        // wyslanie danych do JSP
        RequestDispatcher dispatcher = request.getRequestDispatcher("/admin_view.jsp");
        dispatcher.forward(request, response);

    }
    private void deleteReservation(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // odczytanie danych z formularza
        String id = request.getParameter("reservationID");

        // usuniecie telefonu z BD
        dbUtil.deleteReservation(id);

        // wyslanie danych do strony z lista telefonow
        listReservations(request, response);

    }
}
