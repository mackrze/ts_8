package Java;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@WebServlet("/ClientServlet")
public class ClientServlet extends HttpServlet {

    private DataSource dataSource;
    private DBUtilClient dbUtil;

    public ClientServlet() {

        // Obtain our environment naming context
        Context initCtx = null;
        try {
            initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env");
            // Look up our data source
            dataSource = (DataSource)
                    envCtx.lookup("baza");
            System.out.println("konstr Client servlet");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void init() throws ServletException {
        super.init();

        try {

            dbUtil = new DBUtilClient(dataSource);
            System.out.println("Jest init ");
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            System.out.println("zaczyna get");
            //LocalDate data_od = LocalDate.parse(request.getParameter("data_od"));
            //LocalDate data_do = LocalDate.parse(request.getParameter("data_do"));

            Reservation reservation = new Reservation(request.getParameter("imie_nazwisko"),request.getParameter("e-mail"),request.getParameter("rodzaj_pokoju"), LocalDate.parse(request.getParameter("data_do")),LocalDate.parse(request.getParameter("data_od")));
            System.out.println("jest rezerwacja");
            dbUtil.addReservation(reservation);
            System.out.println("dodal rezerwacje");
            listReservations(request,response);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private void listReservations(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // pobranie danych dzieki DBUtil
        List<Reservation> reservations = dbUtil.getReservation();
        System.out.println("pobral rezewacje");
        // dodanie telefonow do zadania
        request.setAttribute("RESERVATIONS_LIST", reservations);
        System.out.println("sa atrybuty");
        // wyslanie danych do JSP
        RequestDispatcher dispatcher = request.getRequestDispatcher("/client_view.jsp");
        dispatcher.forward(request, response);

    }

}//end class
