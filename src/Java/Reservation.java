package Java;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;

public class Reservation {
    private int id;
    private String imieNazwisko;
    private String email;
    private String room;
    private LocalDate data_od;
    private LocalDate data_do;


    //metoda liczaca cene za pobyt
    public double cenaZaPobyt(){
        long noOfDaysBetween = ChronoUnit.DAYS.between(data_do, data_od);
        HashMap<String,Double> map = new HashMap<>();
        map.put("1-os.",349.99);
        map.put("2-os.",599.99);
        map.put("4-os.",799.99);

        return (double) Math.round(100*noOfDaysBetween*map.get(room).doubleValue())/100;
    }

    public Reservation(int id, String imieNazwisko, String email, String room, LocalDate data_od, LocalDate data_do) {
        this.id = id;
        this.imieNazwisko = imieNazwisko;
        this.email = email;
        this.room = room;
        this.data_od = data_od;
        this.data_do = data_do;
    }

    public Reservation(String imieNazwisko, String email, String room, LocalDate data_od, LocalDate data_do) {
        this.imieNazwisko = imieNazwisko;
        this.email = email;
        this.room = room;
        this.data_od = data_od;
        this.data_do = data_do;
    }

    public Reservation() {
    }

    public String getImieNazwisko() {
        return imieNazwisko;
    }

    public void setImieNazwisko(String imieNazwisko) {
        this.imieNazwisko = imieNazwisko;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public LocalDate getData_od() {
        return data_od;
    }

    public void setData_od(LocalDate data_od) {
        this.data_od = data_od;
    }

    public LocalDate getData_do() {
        return data_do;
    }

    public void setData_do(LocalDate data_do) {
        this.data_do = data_do;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", imieNazwisko='" + imieNazwisko + '\'' +
                ", email='" + email + '\'' +
                ", room='" + room + '\'' +
                ", data_od=" + data_od +
                ", data_do=" + data_do +
                '}';
    }
}
