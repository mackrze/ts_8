<%--
  Created by IntelliJ IDEA.
  User: mackr
  Date: 04.05.2020
  Time: 17:14
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="Java.Reservation, java.util.*"
         import="Java.Room, java.util.*"
         import="Java.Client, java.util.*"
%>
<html>
<head>
    <title>Panel admina</title>
</head>
<body>


<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>


<h1>Telefony</h1>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>

<table class="table table-striped">

    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Imie Nazwisko </th>
        <th scope="col">Email</th>
        <th scope="col">Pokój</th>
        <th scope="col">Data przyjazdu</th>
        <th scope="col">Data wyjazdu</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach var="tmpReservation" items="${RESERVATIONS_LIST}">

       

        <c:url var="deleteLink" value="AdminServlet">
            <c:param name="command" value="DELETE"></c:param>
            <c:param name="reservationID" value="${tmpReservation.id}"></c:param>
        </c:url>


        <tr>
            <th scope="row">${tmpReservation.id}</th>
                <a href="${deleteLink}"
                   onclick="if(!(confirm('Czy na pewno chcesz usunąć ten telefon?'))) return false">
                    <button type="button" class="btn btn-danger">Usuń</button>
                </a></td>
        </tr>

    </c:forEach>
    </tbody>
</table>


</body>
</html>
