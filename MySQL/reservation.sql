create database if not exists lista8TS;
use lista8TS;

drop table if exists reservation;
create table if not exists reservation (
id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
imieNazwisko varchar (50) ,
email varchar (30) ,
room varchar(12) ,
data_od Date ,
data_do Date );

insert into reservation ( id,imieNazwisko,email,room,data_od,data_do) values 
(1,"Adam Krzak","adam@poczta.pl","1-os.","2020-04-18","2020-04-27"),
(2,"Magda Aruk","magda@poczta.pl","2-os.","2020-05-01","2020-05-05");



#CREATE USER 'client'@'localhost' IDENTIFIED BY 'password'; 
#GRANT insert,select ON  lista8ts.reservation TO 'client'@'localhost'; FLUSH PRIVILEGES; 